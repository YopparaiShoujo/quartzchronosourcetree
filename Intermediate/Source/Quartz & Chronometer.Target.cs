using UnrealBuildTool;

public class Quartz & ChronometerTarget : TargetRules
{
	public Quartz & ChronometerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Quartz & Chronometer");
	}
}
